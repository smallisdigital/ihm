package com.smallisdigital.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class InterventionController {

    @Value("${welcome.message}")
    private String message;

    @GetMapping(value = {"/intervention"})
    public String intervention() {
        return "intervention";
    }

    @GetMapping(value = {"/intervention-1"})
    public String intervention1() {
        return "intervention-1";
    }

    @GetMapping(value = {"/intervention-2"})
    public String intervention2() {
        return "intervention-2";
    }

    @GetMapping(value = {"/intervention-3"})
    public String intervention3() {
        return "intervention-3";
    }

    @GetMapping(value = {"/intervention-4"})
    public String intervention4() {
        return "intervention-4";
    }
}
