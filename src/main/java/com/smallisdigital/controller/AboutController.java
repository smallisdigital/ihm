package com.smallisdigital.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AboutController {

    @Value("${welcome.message}")
    private String message;

    @GetMapping(value = {"/about"})
    public String about(Model model) {

        model.addAttribute("message", message);

        return "about";
    }
}
